export default class MoneyAccountController {
  static getAllMoneyAccount () {
    return [
      {
        accountId: 1,
        name: 'testowe',
        idUser: 1
      },
      {
        accountId: 2,
        name: 'testowe2',
        idUser: 1
      }
    ]
  }

  static editMoneyAccount (id, name) {
    console.log('Edited account: ', id, name)
    return true
  }

  static deleteMoneyAccount (id) {
    console.log('Delete account: ', id)
    return true
  }

  static addnewMoneyAccount (name) {
    console.log(name)
    return true
  }
}

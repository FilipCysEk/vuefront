import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { i18n } from './assets/i18n'
import { BootstrapVue } from 'bootstrap-vue'
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon'

import './assets/css/main.scss'
// import 'vue-material-design-icons/styles.css'
// import './assets/css/main.css'
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'

// Layouts
import Default from '@/layouts/Default'
import UserLogged from '@/layouts/UserLogged'

Vue.component('default-layout', Default)
Vue.component('user-logged-layout', UserLogged)

Vue.component('v-icon', Icon)
Vue.use(BootstrapVue)
Vue.config.productionTip = false

new Vue({
  router,
  i18n,
  render: h => h(App)
}).$mount('#app')

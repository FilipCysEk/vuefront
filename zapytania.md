### Logowanie

```javascript
axios({
	method:'POST',
	url: 'http://192.168.0.9:9000/auth',
    withCredentials:true,
	headers:{},
	data: {
		username:'bbzietek215@gmail.com',
		password: '123laptop'
	}
})
.then(r => {
	console.log(r)
})
```

**Zwraca**

* 200 - zalogowany poprawnie, token w ciasteczku
* 401 - błąd logowania - złe dane

**UWAGA domena zapytania musi być taka sama, jak domena na której stoi serwer www!**

Token i nazwa uzytkownika są zwracane w ciasteczku.

### Rejestracja

```javascript
axios({
method:'POST',
url: 'http://192.168.0.9:9765/user-service/register',
headers:{},
data: {
	email: 'krzysiek.swal10@gmail.com',
	name: 'Fifix',
	password: 'Kupa5+'
}
})
.then(r => {
	console.log(r)
})
```

Działa:
* 201 - stworzono nowego użyszkodnika
* 409 - uzytkownik istnieje

### Zwracanie użytkownika

```javascript
axios({
method:'GET',
url: 'http://192.168.0.9:9765/user-service/findUser/bbzietek215@gmail.com',
headers:{
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJiYnppZXRlazIxNUBnbWFpbC5jb20iLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIiwiUk9MRV9VU0VSIl0sImlhdCI6MTU5MDE2MDEzMywiZXhwIjoxNTkwMTYxMDMzfQ.gSxYkpz2FqmUFxLtRfZ0DimWEhzilKoPYAfST7418HR2PI2zFrLTGW0GvB_WGmW_OcyntETduMV0BLWykQNdgA'
},
})
.then(r => {
	console.log(r)
})
```

W docku trzeba zmienić id na username, albo email.
Użytkownicy mogą pobierać informacje o innych użytkownikach?

Zwraca:
* 401 - bez podania authorize
* 200 - poprawny token i usera


### Zmiana usera

```javascript
axios({
method:'PUT',
url: 'http://192.168.0.9:9765/user-service/changeUser?currentPassword=123laptop',
headers:{
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJiYnppZXRlazIxNUBnbWFpbC5jb20iLCJhdXRob3JpdGllcyI6WyJST0xFX0FETUlOIiwiUk9MRV9VU0VSIl0sImlhdCI6MTU5MDMxNzEyMCwiZXhwIjoxNTkwMzE4MDIwfQ.y-DwH7EWDlhwZZTJ22345caBVVWk22JJMAG4-_q-teIXxJVuU2X75tc7xhl_uRTGo7jKX7SIZBQYlg-KAx97rw'
},
data: {
        id:1,
        email: 'bbzietek215@gmail.com',
        name: 'Fifixx',
        password: '123laptop'
    }
})
.then(r => {
	console.log(r)
})
.catch(r => {
	console.log(r)
})
```

Zwraca status `500` i wiadomość:

```json
{
    "timestamp":"2020-05-22T15:17:57.432+0000",
    "status":500,
    "error":"Internal Server Error",
    "message":"The given id must not be null!; nested exception is java.lang.IllegalArgumentException: The given id must not be null!",
    "path":"/changeUser"
}
```

### Usuwanie usera

```javascript
axios({
method:'DELETE',
url: 'http://192.168.0.9:9765/user-service/deleteUser/krzysiek.swal10@gmail.com',
headers:{
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c2llay5zd2FsMTBAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOltdLCJpYXQiOjE1OTAxNjM0NTUsImV4cCI6MTU5MDE2NDM1NX0.z08Z29aC1F0Ok9mWf0xsi_CJ8Pay368HX_Ju_qQAVGxLk-jIY1NDkaxJMBjyHt66gGcb6B1v2JFVU0fpZhZLfg'
},
data: {
	passsword: 'Kupa5+'
}
})
.then(r => {
	console.log(r)
})
.catch(r => {
	console.log(r)
})
```

W docku trzeba zmienić id na username, albo email.
* 401 - unauthorized

Zwróciło kod `400`:
```json
{
    "timestamp":"2020-05-22T15:28:41.386+0000",
    "status":400,
    "error":"Bad Request",
    "message":"Failed to convert value of type 'java.lang.String' to required type 'java.lang.Long'; nested exception is java.lang.NumberFormatException: For input string: \"krzysiek.swal10@gmail.com\"",
    "path":"/deleteUser/krzysiek.swal10@gmail.com"
}
```

```javascript
axios({
method:'DELETE',
url: 'http://192.168.0.9:9765/user-service/deleteUser/5',
headers:{
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c2llay5zd2FsMTBAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOltdLCJpYXQiOjE1OTAxNjEyOTAsImV4cCI6MTU5MDE2MjE5MH0.o6xlkjDgqXd9ODJ4P8PfniHCV_VUkjbYcDEm52RkDOUaIHynfSAF4b891y6ZRfbVuV1Qw147sVZwBx5Qqd3vxg'
},
data: {
	passsword: 'Kupa5+'
}
})
.then(r => {
	console.log(r)
})
.catch(r => {
	console.log(r)
})
```

Zwraca:
```json
{
    "timestamp":"2020-05-22T15:29:48.328+0000",
    "status":409,
    "error":"Conflict",
    "message":"Current password is wrong",
    "path":"/deleteUser/5"
}
```

### Pobranie listy wydatków

```javascript
axios({
method:'GET',
url: 'http://192.168.0.9:9765/expense-account-service/getExpences/5',
headers:{
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c2llay5zd2FsMTBAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOltdLCJpYXQiOjE1OTAxNjM0NTUsImV4cCI6MTU5MDE2NDM1NX0.z08Z29aC1F0Ok9mWf0xsi_CJ8Pay368HX_Ju_qQAVGxLk-jIY1NDkaxJMBjyHt66gGcb6B1v2JFVU0fpZhZLfg'
},
data: {
	from: new Date(2020,1,1),
    to: new Date()
}
})
.then(r => {
	console.log(r)
})
.catch(r => {
	console.log(r)
})
``` 

Zwróciło `404`

### Utworzenie nowego wydatku

```javascript
axios({
method:'POST',
url: 'http://192.168.0.9:9765/expense-account-service/createExpense',
headers:{
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c2llay5zd2FsMTBAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOltdLCJpYXQiOjE1OTAxNjM0NTUsImV4cCI6MTU5MDE2NDM1NX0.z08Z29aC1F0Ok9mWf0xsi_CJ8Pay368HX_Ju_qQAVGxLk-jIY1NDkaxJMBjyHt66gGcb6B1v2JFVU0fpZhZLfg'
},
data: {
	account:1,
    idUser:5,
    amount:250,
    date: new Date(),
    note: 'To bylo ważne',
    subcategory:1,
    type: 'EXPENSE'
}
})
.then(r => {
	console.log(r)
})
.catch(r => {
	console.log(r)
})
``` 

Zwróciło `404`


### Przelanie środków

```javascript
axios({
method:'POST',
url: 'http://192.168.0.9:9765/expense-account-service/transfer/5',
headers:{
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c2llay5zd2FsMTBAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOltdLCJpYXQiOjE1OTAxNjIyNzgsImV4cCI6MTU5MDE2MzE3OH0.a9gym-h4A9QHOMpo7uRi959o0uqLbqFESVnV5S9XHkgFfECn3_gHoxEegGy725ocdlxGiHje1gSpWS-30uF0Cg'
},
data: {
	fromAcoountId:1,
    toAcoountId:2,
    amount:250
}
})
.then(r => {
	console.log(r)
})
.catch(r => {
	console.log(r)
})
``` 

Zwrócilo `404`


### Pobranie konta

```javascript
axios({
method:'GET',
url: 'http://192.168.0.9:9765/expense-account-service/getAccount/1',
headers:{
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c2llay5zd2FsMTBAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOltdLCJpYXQiOjE1OTAxNjIyNzgsImV4cCI6MTU5MDE2MzE3OH0.a9gym-h4A9QHOMpo7uRi959o0uqLbqFESVnV5S9XHkgFfECn3_gHoxEegGy725ocdlxGiHje1gSpWS-30uF0Cg'
}
})
.then(r => {
	console.log(r)
})
.catch(r => {
	console.log(r)
})
``` 

Zwróciło `404`

### Pobieranie wszystkich kont

```javascript
axios({
method:'GET',
url: 'http://192.168.0.9:9765/expense-account-service/getAllUserAccounts/5',
headers:{
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c2llay5zd2FsMTBAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOltdLCJpYXQiOjE1OTAxNjIyNzgsImV4cCI6MTU5MDE2MzE3OH0.a9gym-h4A9QHOMpo7uRi959o0uqLbqFESVnV5S9XHkgFfECn3_gHoxEegGy725ocdlxGiHje1gSpWS-30uF0Cg'
}
})
.then(r => {
	console.log(r)
})
.catch(r => {
	console.log(r)
})
```

Zwróciło `404`

### Utworzenie nowego konta

```javascript
axios({
method:'PUT',
url: 'http://192.168.0.9:9765/expense-account-service/createAccount',
headers:{
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c2llay5zd2FsMTBAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOltdLCJpYXQiOjE1OTAxNjIyNzgsImV4cCI6MTU5MDE2MzE3OH0.a9gym-h4A9QHOMpo7uRi959o0uqLbqFESVnV5S9XHkgFfECn3_gHoxEegGy725ocdlxGiHje1gSpWS-30uF0Cg'
},
data: {
    name:'Głupoty',
    idUser:5,
    is_visible: true
}
})
.then(r => {
	console.log(r)
})
.catch(r => {
	console.log(r)
})
```

is_visible, powinno byc ustawiane przez serwer!
Zwróciło `404`

### Usunięcie konta

```javascript
axios({
method:'DELETE',
url: 'http://192.168.0.9:9765/expense-account-service/deleteAccount/1',
headers:{
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c2llay5zd2FsMTBAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOltdLCJpYXQiOjE1OTAxNjIyNzgsImV4cCI6MTU5MDE2MzE3OH0.a9gym-h4A9QHOMpo7uRi959o0uqLbqFESVnV5S9XHkgFfECn3_gHoxEegGy725ocdlxGiHje1gSpWS-30uF0Cg'
}
})
.then(r => {
	console.log(r)
})
.catch(r => {
	console.log(r)
})
```

Zwrociło `404`


### Pobiranie kategorii

```javascript
axios({
method:'DELETE',
url: 'http://192.168.0.9:9765/expense-account-service/deleteAccount/1',
headers:{
    'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJrcnp5c2llay5zd2FsMTBAZ21haWwuY29tIiwiYXV0aG9yaXRpZXMiOltdLCJpYXQiOjE1OTAxNjIyNzgsImV4cCI6MTU5MDE2MzE3OH0.a9gym-h4A9QHOMpo7uRi959o0uqLbqFESVnV5S9XHkgFfECn3_gHoxEegGy725ocdlxGiHje1gSpWS-30uF0Cg'
}
})
.then(r => {
	console.log(r)
})
.catch(r => {
	console.log(r)
})
```


Reszty nie sprawdzałem, bo mi się nie chciało, bo wszystkie walą 404
